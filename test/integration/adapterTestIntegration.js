/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-paragon_dpm',
      type: 'ParagonDpm',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const ParagonDpm = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Paragon_dpm Adapter Test', () => {
  describe('ParagonDpm Class Tests', () => {
    const a = new ParagonDpm(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const dpmServiceDpmServiceBulkExtRefUpdateBodyParam = {
      items: [
        {
          'ref-from-type': 'string',
          'ref-from-uuid': 'string',
          operation: 'string',
          'ref-to-uuid': 'string',
          'ref-to-type': 'string'
        }
      ]
    };
    describe('#dpmServiceBulkExtRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceBulkExtRefUpdate(dpmServiceDpmServiceBulkExtRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceBulkExtRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceBulkListCaCertificateBlobBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: false,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: true,
        exclude_hrefs: false,
        tag_detail: false,
        count: false,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#dpmServiceBulkListCaCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceBulkListCaCertificateBlob(dpmServiceDpmServiceBulkListCaCertificateBlobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceBulkListCaCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceBulkListCertificateBlobBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: true,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: false,
        exclude_hrefs: true,
        tag_detail: true,
        count: false,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#dpmServiceBulkListCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceBulkListCertificateBlob(dpmServiceDpmServiceBulkListCertificateBlobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceBulkListCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceBulkListDeletedResourceBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: true,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: true,
        exclude_hrefs: true,
        tag_detail: false,
        count: true,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#dpmServiceBulkListDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceBulkListDeletedResource(dpmServiceDpmServiceBulkListDeletedResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceBulkListDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceBulkListImageBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: true,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: true,
        exclude_hrefs: false,
        tag_detail: true,
        count: true,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#dpmServiceBulkListImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceBulkListImage(dpmServiceDpmServiceBulkListImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceBulkListImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceBulkListLastPublishedNotificationBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: true,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: true,
        exclude_hrefs: false,
        tag_detail: true,
        count: true,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#dpmServiceBulkListLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceBulkListLastPublishedNotification(dpmServiceDpmServiceBulkListLastPublishedNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceBulkListLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceBulkRefUpdateBodyParam = {
      refs: [
        {
          'ref-type': 'string',
          'ref-uuid': 'string',
          operation: 'string',
          type: 'string',
          uuid: 'string'
        }
      ],
      type: 'string',
      uuid: 'string'
    };
    describe('#dpmServiceBulkRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceBulkRefUpdate(dpmServiceDpmServiceBulkRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceBulkRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceCreateCaCertificateBlobBodyParam = {
      'ca-certificate-blob': {
        updated_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        device_uuid: 'string',
        href: 'string',
        certificate_contents: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: true,
          user_visible: true
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        parent_uuid: 'string'
      }
    };
    describe('#dpmServiceCreateCaCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceCreateCaCertificateBlob(dpmServiceDpmServiceCreateCaCertificateBlobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceCreateCaCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceCreateCertificateBlobBodyParam = {
      'certificate-blob': {
        updated_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        device_uuid: 'string',
        href: 'string',
        certificate_contents: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: true,
          user_visible: true
        },
        created_by: 'string',
        certificate_key_contents: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        parent_uuid: 'string'
      }
    };
    describe('#dpmServiceCreateCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceCreateCertificateBlob(dpmServiceDpmServiceCreateCertificateBlobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceCreateCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceCreateDeletedResourceBodyParam = {
      'deleted-resource': {
        updated_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        href: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: false,
          user_visible: false
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        data: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        resource_type: 'string',
        parent_uuid: 'string'
      }
    };
    describe('#dpmServiceCreateDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceCreateDeletedResource(dpmServiceDpmServiceCreateDeletedResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceCreateDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceExtRefUpdateBodyParam = {
      'ref-from-type': 'string',
      'ref-from-uuid': 'string',
      operation: 'string',
      'ref-to-uuid': 'string',
      'ref-to-type': 'string'
    };
    describe('#dpmServiceExtRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceExtRefUpdate(dpmServiceDpmServiceExtRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceExtRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceCreateImageBodyParam = {
      image: {
        parent_uuid: 'string',
        parent_type: 'string',
        href: 'string',
        created_timestamp: 'string',
        updated_by: 'string',
        updated_timestamp: 'string',
        uuid: 'string',
        configuration_version: 'string',
        image_type: 'string',
        created_by: 'string',
        version: 'string',
        supported_platforms: [
          'string'
        ],
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        vendor: 'string',
        description: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        file_id: 'string',
        image_size: 'string',
        device: [
          {
            component: 'string',
            device_uuid: 'string'
          }
        ],
        name: 'string',
        upload_timestamp: 'string',
        file_uri: 'string',
        sha1_hash: 'string',
        device_family: [
          'string'
        ],
        meta: {
          enable: true,
          user_visible: false
        }
      }
    };
    describe('#dpmServiceCreateImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceCreateImage(dpmServiceDpmServiceCreateImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceCreateImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceCreateLastPublishedNotificationBodyParam = {
      'last-published-notification': {
        updated_timestamp: 'string',
        last_published_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        href: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: true,
          user_visible: false
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        resource_type: 'string',
        parent_uuid: 'string'
      }
    };
    describe('#dpmServiceCreateLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceCreateLastPublishedNotification(dpmServiceDpmServiceCreateLastPublishedNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceCreateLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceRefUpdateBodyParam = {
      'ref-type': 'string',
      'ref-uuid': 'string',
      operation: 'string',
      type: 'string',
      uuid: 'string'
    };
    describe('#dpmServiceRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceRefUpdate(dpmServiceDpmServiceRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceSyncBodyParam = {
      'last-published-notification': {
        resources: [
          {
            updated_timestamp: 'string',
            last_published_timestamp: 'string',
            description: 'string',
            configuration_version: 'string',
            href: 'string',
            parent_type: 'string',
            uuid: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            meta: {
              enable: false,
              user_visible: true
            },
            created_by: 'string',
            updated_by: 'string',
            created_timestamp: 'string',
            annotations: {
              key_value_pair: [
                {
                  key: 'string',
                  value: 'string'
                }
              ]
            },
            resource_type: 'string',
            parent_uuid: 'string'
          }
        ]
      },
      'certificate-blob': {
        resources: [
          {
            updated_timestamp: 'string',
            description: 'string',
            configuration_version: 'string',
            device_uuid: 'string',
            href: 'string',
            certificate_contents: 'string',
            parent_type: 'string',
            uuid: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            meta: {
              enable: true,
              user_visible: false
            },
            created_by: 'string',
            certificate_key_contents: 'string',
            updated_by: 'string',
            created_timestamp: 'string',
            annotations: {
              key_value_pair: [
                {
                  key: 'string',
                  value: 'string'
                }
              ]
            },
            parent_uuid: 'string'
          }
        ]
      },
      'ca-certificate-blob': {
        resources: [
          {
            updated_timestamp: 'string',
            description: 'string',
            configuration_version: 'string',
            device_uuid: 'string',
            href: 'string',
            certificate_contents: 'string',
            parent_type: 'string',
            uuid: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            meta: {
              enable: true,
              user_visible: true
            },
            created_by: 'string',
            updated_by: 'string',
            created_timestamp: 'string',
            annotations: {
              key_value_pair: [
                {
                  key: 'string',
                  value: 'string'
                }
              ]
            },
            parent_uuid: 'string'
          }
        ]
      },
      image: {
        resources: [
          {
            parent_uuid: 'string',
            parent_type: 'string',
            href: 'string',
            created_timestamp: 'string',
            updated_by: 'string',
            updated_timestamp: 'string',
            uuid: 'string',
            configuration_version: 'string',
            image_type: 'string',
            created_by: 'string',
            version: 'string',
            supported_platforms: [
              'string'
            ],
            annotations: {
              key_value_pair: [
                {
                  key: 'string',
                  value: 'string'
                }
              ]
            },
            vendor: 'string',
            description: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            file_id: 'string',
            image_size: 'string',
            device: [
              {
                component: 'string',
                device_uuid: 'string'
              }
            ],
            name: 'string',
            upload_timestamp: 'string',
            file_uri: 'string',
            sha1_hash: 'string',
            device_family: [
              'string'
            ],
            meta: {
              enable: false,
              user_visible: true
            }
          }
        ]
      },
      ignore_optimistic_lock: false,
      operation: 'string',
      'deleted-resource': {
        resources: [
          {
            updated_timestamp: 'string',
            description: 'string',
            configuration_version: 'string',
            href: 'string',
            parent_type: 'string',
            uuid: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            meta: {
              enable: true,
              user_visible: true
            },
            created_by: 'string',
            updated_by: 'string',
            created_timestamp: 'string',
            data: 'string',
            annotations: {
              key_value_pair: [
                {
                  key: 'string',
                  value: 'string'
                }
              ]
            },
            resource_type: 'string',
            parent_uuid: 'string'
          }
        ]
      },
      field_mask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#dpmServiceSync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceSync(dpmServiceDpmServiceSyncBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceListCaCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceListCaCertificateBlob(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceListCaCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceID = 'fakedata';
    const dpmServiceDpmServiceUpdateCaCertificateBlobBodyParam = {
      'ca-certificate-blob': {
        updated_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        device_uuid: 'string',
        href: 'string',
        certificate_contents: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: false,
          user_visible: false
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        parent_uuid: 'string'
      },
      ignore_optimistic_lock: false,
      ID: 'string',
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#dpmServiceUpdateCaCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceUpdateCaCertificateBlob(dpmServiceID, dpmServiceDpmServiceUpdateCaCertificateBlobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceUpdateCaCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceGetCaCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceGetCaCertificateBlob(dpmServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceGetCaCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceListCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceListCertificateBlob(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceListCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceUpdateCertificateBlobBodyParam = {
      ignore_optimistic_lock: false,
      ID: 'string',
      fieldMask: {
        paths: [
          'string'
        ]
      },
      'certificate-blob': {
        updated_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        device_uuid: 'string',
        href: 'string',
        certificate_contents: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: false,
          user_visible: true
        },
        created_by: 'string',
        certificate_key_contents: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        parent_uuid: 'string'
      }
    };
    describe('#dpmServiceUpdateCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceUpdateCertificateBlob(dpmServiceID, dpmServiceDpmServiceUpdateCertificateBlobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceUpdateCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceGetCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceGetCertificateBlob(dpmServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceGetCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceListDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceListDeletedResource(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceListDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceUpdateDeletedResourceBodyParam = {
      ignore_optimistic_lock: true,
      'deleted-resource': {
        updated_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        href: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: false,
          user_visible: true
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        data: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        resource_type: 'string',
        parent_uuid: 'string'
      },
      ID: 'string',
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#dpmServiceUpdateDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceUpdateDeletedResource(dpmServiceID, dpmServiceDpmServiceUpdateDeletedResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceUpdateDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceGetDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceGetDeletedResource(dpmServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceGetDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceListImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceListImage(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceListImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceUpdateImageBodyParam = {
      image: {
        parent_uuid: 'string',
        parent_type: 'string',
        href: 'string',
        created_timestamp: 'string',
        updated_by: 'string',
        updated_timestamp: 'string',
        uuid: 'string',
        configuration_version: 'string',
        image_type: 'string',
        created_by: 'string',
        version: 'string',
        supported_platforms: [
          'string'
        ],
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        vendor: 'string',
        description: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        file_id: 'string',
        image_size: 'string',
        device: [
          {
            component: 'string',
            device_uuid: 'string'
          }
        ],
        name: 'string',
        upload_timestamp: 'string',
        file_uri: 'string',
        sha1_hash: 'string',
        device_family: [
          'string'
        ],
        meta: {
          enable: false,
          user_visible: true
        }
      },
      ID: 'string',
      ignore_optimistic_lock: false,
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#dpmServiceUpdateImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceUpdateImage(dpmServiceID, dpmServiceDpmServiceUpdateImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceUpdateImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceGetImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceGetImage(dpmServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceGetImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceListLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceListLastPublishedNotification(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceListLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dpmServiceDpmServiceUpdateLastPublishedNotificationBodyParam = {
      ignore_optimistic_lock: true,
      'last-published-notification': {
        updated_timestamp: 'string',
        last_published_timestamp: 'string',
        description: 'string',
        configuration_version: 'string',
        href: 'string',
        parent_type: 'string',
        uuid: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        meta: {
          enable: false,
          user_visible: true
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              key: 'string',
              value: 'string'
            }
          ]
        },
        resource_type: 'string',
        parent_uuid: 'string'
      },
      ID: 'string',
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#dpmServiceUpdateLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceUpdateLastPublishedNotification(dpmServiceID, dpmServiceDpmServiceUpdateLastPublishedNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceUpdateLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceGetLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceGetLastPublishedNotification(dpmServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceGetLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerAddImageBodyParam = {
      vendor: 'string',
      name: 'string',
      image_type: 'string',
      image_file_uri: 'string',
      supported_platform: [
        'string'
      ],
      sha1_hash: 'string',
      device_family: [
        'string'
      ],
      version: 'string',
      description: 'string'
    };
    describe('#imageManagerAddImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerAddImage(imageManagerImageManagerAddImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerAddImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerDeleteCertificateBodyParam = {
      certificate_type: 'string',
      certificate_id: 'string',
      next_run_time: 'string',
      device_uuid: 'string'
    };
    describe('#imageManagerDeleteCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerDeleteCertificate(imageManagerImageManagerDeleteCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerDeleteCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerDeleteImagesBodyParam = {
      image_uuids: [
        'string'
      ]
    };
    describe('#imageManagerDeleteImages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerDeleteImages(imageManagerImageManagerDeleteImagesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerDeleteImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerDeployCACertificateBodyParam = {
      certificate_contents: 'string',
      device_uuid: 'string',
      next_run_time: 'string',
      ca_profile_id: 'string'
    };
    describe('#imageManagerDeployCACertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerDeployCACertificate(imageManagerImageManagerDeployCACertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerDeployCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerDeployImageBodyParam = {
      device: [
        {
          image_uuid: 'string',
          device_uuid: 'string',
          device_component_name: 'string'
        }
      ],
      next_run_time: 'string'
    };
    describe('#imageManagerDeployImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerDeployImage(imageManagerImageManagerDeployImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerDeployImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerDeployLicenseBodyParam = {
      next_run_time: 'string',
      primary_license_contents: 'string',
      device_uuid: 'string',
      license_contents: 'string',
      secondary_license_contents: 'string'
    };
    describe('#imageManagerDeployLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerDeployLicense(imageManagerImageManagerDeployLicenseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerDeployLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerDeployLocalCertificateBodyParam = {
      next_run_time: 'string',
      certificate_key_contents: 'string',
      device_uuid: 'string',
      certificate_id: 'string',
      cert_file: 'string',
      pass_phrase: 'string',
      'key-file': 'string',
      certificate_contents: 'string'
    };
    describe('#imageManagerDeployLocalCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerDeployLocalCertificate(imageManagerImageManagerDeployLocalCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerDeployLocalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerEnableDefaultCAProfileBodyParam = {
      ca_group_name: 'string',
      device_uuids: [
        'string'
      ]
    };
    describe('#imageManagerEnableDefaultCAProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerEnableDefaultCAProfile(imageManagerImageManagerEnableDefaultCAProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerEnableDefaultCAProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerStageImageBodyParam = {
      device: [
        {
          image_uuid: 'string',
          device_uuid: 'string',
          device_component_name: 'string'
        }
      ],
      next_run_time: 'string'
    };
    describe('#imageManagerStageImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerStageImage(imageManagerImageManagerStageImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerStageImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageManagerImageManagerUploadImageURLBodyParam = {
      url: 'string'
    };
    describe('#imageManagerUploadImageURL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageManagerUploadImageURL(imageManagerImageManagerUploadImageURLBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImageManager', 'imageManagerUploadImageURL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceDeleteCaCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceDeleteCaCertificateBlob(dpmServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceDeleteCaCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceDeleteCertificateBlob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceDeleteCertificateBlob(dpmServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceDeleteCertificateBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceDeleteDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceDeleteDeletedResource(dpmServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceDeleteDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceDeleteImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceDeleteImage(dpmServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceDeleteImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dpmServiceDeleteLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dpmServiceDeleteLastPublishedNotification(dpmServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_dpm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DpmService', 'dpmServiceDeleteLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
