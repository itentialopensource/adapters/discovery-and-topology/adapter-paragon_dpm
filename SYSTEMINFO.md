# Paragon DPM

Vendor: Juniper
Homepage: https://www.juniper.net

Product: DPM (Paragon Automation)
Product Page: https://www.juniper.net/documentation/product/us/en/paragon-automation/

## Introduction
We classify Paragon DPM into the Discovery & Topology domain as Paragon DPM provides capabilities to enable the discovery and management of network devices and their configuration.

## Why Integrate
The Paragon DPM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Paragon DPM. With this adapter you have the ability to perform operations on items such as:

- DPM Service
- Image Manager

## Additional Product Documentation