
## 0.3.4 [10-14-2024]

* Changes made at 2024.10.14_18:39PM

See merge request itentialopensource/adapters/adapter-paragon_dpm!12

---

## 0.3.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-paragon_dpm!10

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:20PM

See merge request itentialopensource/adapters/adapter-paragon_dpm!9

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_18:20PM

See merge request itentialopensource/adapters/adapter-paragon_dpm!8

---

## 0.3.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dpm!7

---

## 0.2.5 [03-25-2024]

* Changes made at 2024.03.25_11:03AM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dpm!6

---

## 0.2.4 [03-13-2024]

* Changes made at 2024.03.13_12:46PM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dpm!5

---

## 0.2.3 [03-11-2024]

* Update Metadata

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dpm!4

---

## 0.2.2 [02-26-2024]

* Changes made at 2024.02.26_10:57AM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dpm!3

---

## 0.2.1 [12-29-2023]

* fix metadata

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dpm!2

---

## 0.2.0 [12-29-2023]

* More migration changes

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_dpm!1

---

## 0.1.1 [10-03-2023]

* Bug fixes and performance improvements

See commit 2c8f379

---
